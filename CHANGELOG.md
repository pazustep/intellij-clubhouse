# Changelog

## 0.1.13 - 2022-06-06
- Support Jetbrains Platform 2022.2 and later.

## 0.1.12 - 2022-03-14
- Support Jetbrains Platform 2022.1.
- Fix [#8](https://gitlab.com/pazustep/intellij-clubhouse/-/issues/8) — "Cannot connect" error when searching for issues. 

## 0.1.11 - 2021-10-18
- Fix saving server preferences. Shortcut servers are now correctly saved when
  closing projects.

## 0.1.10 - 2021-09-24
- Support Jetbrains Platform 2021.3.

## 0.1.9 - 2021-09-23
- Update story ids to `sc-1234` format.

## 0.1.8 - 2021-09-15
- Updated branding from Clubhouse to Shortcut.
- Updated minimum JetBrains platform version to 2021.1.

## 0.1.7 — 2021-06-18
- Support Jetbrains Platform 2021.2.

## 0.1.6
- Support Jetbrains Platform 2021.1.

## 0.1.5
- Support Jetbrains Platform 2020.3.
- ${id} template substitutions now resolve to `ch$id`.

## 0.1.4
- 2020.2 EAP is now supported.
- Don't include epics in search results (Merge Request #1, thanks to Igor Santos).

## 0.1.3
- Compatibility with 2020.1 EAP products.

## 0.1.2
- Cosmetic and internal changes. We have a plugin icon now!

## 0.1.1
- Fetch only tasks owned by the current user if no query is entered.

## 0.1.0
- Initial release of the plugin. It's very bare bones at the moment, but
  you can already fetch your stories.
