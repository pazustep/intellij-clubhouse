package to.bri.intellij.clubhouse

import com.intellij.tasks.Task
import com.intellij.tasks.impl.BaseRepository
import com.intellij.tasks.impl.httpclient.NewBaseRepositoryImpl
import com.intellij.util.xmlb.annotations.Tag
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import okio.buffer
import okio.source
import org.apache.http.HttpRequestInterceptor
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.slf4j.LoggerFactory
import to.bri.intellij.clubhouse.api.MemberInfo
import to.bri.intellij.clubhouse.api.StorySearchResults
import java.io.InterruptedIOException
import java.util.*
import kotlin.math.min

@Tag("Shortcut")
class ClubhouseRepository : NewBaseRepositoryImpl {

    private val logger = LoggerFactory.getLogger(ClubhouseRepository::class.java)

    private val moshi = Moshi.Builder()
        .add(DateAdapter)
        .build()

    private var owner: MemberInfo? = null

    @Suppress("unused")
    constructor() : super()

    constructor(type: ClubhouseRepositoryType?) : super(type)
    constructor(other: ClubhouseRepository?) : super(other)

    override fun clone(): BaseRepository {
        return ClubhouseRepository(this)
    }

    override fun getUrl(): String {
        return "https://api.app.shortcut.com"
    }

    override fun getPresentableName(): String {
        return "Shortcut"
    }

    override fun createRequestInterceptor(): HttpRequestInterceptor {
        return HttpRequestInterceptor { request, _ -> request.addHeader("Shortcut-Token", password) }
    }

    override fun createCancellableConnection(): CancellableConnection {
        val uri = URIBuilder(getRestApiUrl("member")).build()
        return HttpTestConnection(HttpGet(uri))
    }

    override fun getRestApiPathPrefix(): String {
        return "/api/v3"
    }

    @Throws(Exception::class)
    override fun getIssues(search: String?, offset: Int, limit: Int, withClosed: Boolean): Array<Task> {
        return try {
            val tokens: MutableList<String> = ArrayList()
            tokens.add("is:story")

            if (!withClosed) tokens.add("!is:done")

            if (search != null && search.isNotBlank()) {
                tokens.add(search.trim())
            } else {
                getDefaultQuery()?.let { tokens.add(it) }
            }

            val query = tokens.joinToString(separator = " ")

            val searchUri = URIBuilder(getRestApiUrl("search", "stories"))
                .addParameter("query", query)
                .addParameter("page_size", min(25, limit).toString())
                .build()

            val searchMethod = HttpGet(searchUri)

            logger.debug("Requesting stories from clubhouse with query {}", query)
            httpClient.execute(searchMethod, this::parseStories)
        } catch (ie: InterruptedIOException) {
            emptyArray()
        } catch (error: Exception) {
            logger.error("Failed to fetch issues from shortcut", error)
            throw error
        }
    }

    private fun getDefaultQuery(): String? {
        return getOwner()?.mentionName
    }

    private fun getOwner(): MemberInfo? {
        if (owner == null) owner = fetchOwner()
        return owner
    }

    private fun fetchOwner(): MemberInfo? {
        val uri = URIBuilder(getRestApiUrl("member")).build()
        val method = HttpGet(uri)

        return try {
            httpClient.execute(method, this::parseMemberInfo)
        } catch (e: Exception) {
            logger.error("Failed to fetch current member info", e)
            null
        }
    }

    private fun parseMemberInfo(response: HttpResponse): MemberInfo? {
        return if (response.statusLine.statusCode == 200) {
            return response.parseAs<MemberInfo>()
        } else {
            logger.error("Failed to parse current member info: {}", response.statusLine)
            null
        }
    }

    private fun parseStories(response: HttpResponse): Array<Task> {
        return if (response.statusLine.statusCode == 200) {
            val stories = response.parseAs<StorySearchResults>()?.stories ?: emptyList()
            stories.map { story -> ClubhouseTask(story, this) }.toTypedArray()
        } else {
            logger.error("Failed to parse stories: {}", response.statusLine)
            arrayOf()
        }
    }

    private fun <T> HttpResponse.parse(adapter: JsonAdapter<T>): T? {
        return entity.content.source().use { input ->
            input.buffer().use { buffer -> adapter.fromJson(buffer) }
        }
    }

    private inline fun <reified T> HttpResponse.parseAs(): T? {
        return parse(moshi.adapter(T::class.java))
    }

    @Throws(Exception::class)
    override fun findTask(id: String): Task? {
        return null
    }

}
