package to.bri.intellij.clubhouse.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class Comment(

    @Json(name = "id")
    val id: Int,

    @Json(name = "app_url")
    val url: String,

    @Json(name = "text")
    val text: String?,

    @Json(name = "created_at")
    val createdAt: Date

)
