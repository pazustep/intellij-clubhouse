package to.bri.intellij.clubhouse.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MemberInfo(

    @Json(name = "id")
    val id: String,

    @Json(name = "mention_name")
    val mentionName: String,

    @Json(name = "name")
    val name: String

)
