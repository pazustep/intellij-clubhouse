package to.bri.intellij.clubhouse.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StorySearchResults(

    @Json(name = "cursors")
    val cursors: List<String>?,

    @Json(name = "data")
    val stories: List<Story>,

    @Json(name = "next")
    val next: String?,

    @Json(name = "total")
    val total: Int

)
