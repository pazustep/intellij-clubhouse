package to.bri.intellij.clubhouse

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant
import java.util.*

object DateAdapter {

    @FromJson
    fun fromJson(json: String): Date = Date.from(Instant.parse(json))

    @ToJson
    fun toJson(date: Date): String = date.toInstant().toString()

}
