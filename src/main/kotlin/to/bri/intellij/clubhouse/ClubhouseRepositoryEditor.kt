package to.bri.intellij.clubhouse

import com.intellij.openapi.project.Project
import com.intellij.tasks.config.BaseRepositoryEditor
import com.intellij.util.Consumer

class ClubhouseRepositoryEditor(
    project: Project,
    repository: ClubhouseRepository,
    changeListener: Consumer<in ClubhouseRepository>
) : BaseRepositoryEditor<ClubhouseRepository>(
    project,
    repository,
    changeListener
) {

    init {
        myUrlLabel.isVisible = false
        myURLText.isVisible = false
        myUsernameLabel.isVisible = false
        myUserNameText.isVisible = false
        myPasswordLabel.text = "API Token: "
    }

}
